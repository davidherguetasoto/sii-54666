#define NAME_FIFO "fifo_logger"
#include<iostream>
#include<string.h>
#include<stdio.h>
#include<sys/stat.h>
#include<unistd.h>
#include<fcntl.h>

using namespace std;
int main(void)
{
	int fd; //descriptor de la fifo
	char buffer[60];
	
	//Se crea la tuberia
	if(mkfifo(NAME_FIFO,0666)==-1)
		{
			perror(NAME_FIFO":Create()");
			return -1;
		}
	
	//Se abre la tuberia en modo lectura
	fd=open(NAME_FIFO,O_RDONLY);
	if(fd==-1)
	{
		perror(NAME_FIFO":open()");
		return -1;
	}
	while(1)
	{
		if(read(fd,&buffer,sizeof(buffer)))
		{
			if(!strcmp(buffer,"end"))
			{
				if(close(fd)==-1)
				{
					perror(NAME_FIFO":open()");
					return -1;
				}
				if(unlink(NAME_FIFO)==-1)
				{
					perror(NAME_FIFO":unlink()");
					return -1;
				}
				return 0;
			}
			cout<<buffer<<"\n";
		}
	}
}
