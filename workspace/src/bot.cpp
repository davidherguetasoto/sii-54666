#include<stdio.h>
#include<sys/types.h>
#include<sys/mman.h>
#include<fcntl.h>
#include<unistd.h>
#include"DatosMemCompartida.h"

int main()
{
	DatosMemCompartida *data;
	int fd;
	float centro_raqueta;
	
	fd=open("shared_data",O_RDWR);
	if(fd==-1)
	{
		perror("Error abriendo el fichero shared_data");
		exit(-1);
	}
	if((data=(DatosMemCompartida*)mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0))==(void*)MAP_FAILED)
	
	{
		perror("Error al proyectar el fichero shared_data");
		close(fd);
		exit(-1);
	}
	if(close(fd)<0)
	{
		perror("Error al cerrar ficheri shared_data");
		return -1;
	}
	
	while(1)
	{
		usleep(25000);
		
		centro_raqueta=(data->raqueta1.y1+data->raqueta1.y2)/2;
		if(data->esfera.centro.y>centro_raqueta)data->accion=1; else
		if(data->esfera.centro.y<centro_raqueta)data->accion=-1; else
		data->accion=0;
		if(data->end==1)
		{
			if(munmap(data,sizeof(DatosMemCompartida))==-1)
			{
				perror("Error eliminando proyeccion shared_data");
				exit(-1);
			}
			
			return 0;
		}
	}
		
}
